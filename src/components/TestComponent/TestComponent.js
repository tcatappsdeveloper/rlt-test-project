import React from 'react';

const TestComponent = ({ title }) => {


    const onPragraphClicked = () => {
        console.log("Pragraph got clicked");
    }

    const capitaize = title => {
        return title.toUpperCase()
    }

    return (
        <div>
            <p role={"paragraph"} onClick={ onPragraphClicked }>
                {capitaize(title)}
            </p>
        </div>
    );
}

export default TestComponent;