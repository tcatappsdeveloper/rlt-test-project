import { render, screen } from '@testing-library/react';
import Header from '../Header';

test('test one', async () => {
  render(<Header title={"My Header"} />);
  const headingElement = screen.getByText(/my header/i);
  expect(headingElement).toBeInTheDocument();
});

test('test two', async () => {
    render(<Header title={"My Header"} />);
    const headingElement = screen.getByRole("heading", {name : "My Header"});
    expect(headingElement).toBeInTheDocument();
});


test('test three', async () => {
    render(<Header title={"My Header"} />);
    const headingElement = screen.getByRole("heading", {name : "My Header"});
    expect(headingElement).toBeInTheDocument();
});


test('test four', async () => {
  render(<Header title={"My Header"} />);
  const headingElement = screen.getByRole("heading", {name : "My Header"});
  expect(headingElement).toBeInTheDocument();
});


test('test five', async () => {
  render(<Header title={"My Header"} />);
  const headingElement = screen.getByRole("heading", {name : "My Header"});
  expect(headingElement).toBeInTheDocument();
});







